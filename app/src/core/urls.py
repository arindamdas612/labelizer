from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('download-template-1/', download_template_1, name='download_template_1'),
    path('download-template-2/', download_template_2, name='download_template_2'),
]