import pandas as pd
import io

from reportlab.graphics.barcode import qr
from reportlab.graphics.shapes import Drawing 
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from reportlab.graphics import renderPDF
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib.pagesizes import letter, inch
from reportlab.lib import colors

''' For Inner Table '''
# def get_labels_for_table(data):
#     buffer = io.BytesIO()
#     data = data.fillna({'QTY': 0})
#     ''' 100 * 150 '''
#     c = canvas.Canvas(buffer, bottomup=0, pagesize=(160*mm,120*mm))

#     for index, row in data.iterrows():
#         mrp = '{:0,.2f}'.format(row.MRP)
#         product_name = row.Product_Name if row.Product_Name else 'UnNamed Product'
#         for i in range(0, int(row.QTY)):
#             textobject = c.beginText()
#             textobject.setTextOrigin(3*mm, 5*mm)
#             textobject.setFont('Courier', 10)
#             textobject.textLine(text='Big Fox - ' + product_name)
#             c.drawText(textobject)
#             textobject.setFont('Courier', 14)
#             textobject.textLine(text=' ')
#             elements = list()

#             print('--------------------------------------------------')
#             print(row.Manufctured_by)
            
#             col_name = 'Manufctured by \n\n Month Year of \n Manufactioring \n\n Generic Name \n\n MRP \n\n Dimensions\
#                      \n\n Net Quantity \n\n Article Number \n\n Size \n\n Color Code \n\n BRAND \n\n FSN'
            
#             col_value =str(row.Manufctured_by)+'\n\n\n    '+str(row.Month_Year_of_Manufactioring)+'\n\n    '+str(row.Generic_Name)+'\n\n    '+str(row.MRP)+'\n\n    '+\
#             str(row.Dimensions)+'\n\n    '+\
#             str(row.Net_Quantity)+\
#             '\n\n    '+str(row.Article_number)+'\n\n    '+str(row.Size_in_number)+'\n\n    '+\
#             str(row.Color_code) +'\n\n    ' +\
#             str(row.Brand)+'\n\n   '+str(row.FSN)

#             fsn = [col_name, col_value]
#             size = ['Size in Number',row.Size_in_number]
#             data = [fsn]

#             width = 50
#             height = 50
#             x = 70
#             y = 30

#             f=Table(data,2*[2*inch], 1*[3.75*inch])

#             f.setStyle(TableStyle([('INNERGRID', (0,0), (1,1), 1.0, colors.black),
#             ('ALIGN',(0,1),(-1,-1),'CENTER'),
#             ('FONTNAME', (0,0), (0,-1), 'Courier-Bold'),
#             ('FONTSIZE', (0, 1), (-1, -1), 14),
#             ]))
#             f.wrapOn(c, width, height)
#             f.drawOn(c, x, y)
#             c.showPage()
#     c.save()
#     buffer.seek(0)
#     return buffer

def get_tupple_from_dataframe(df):
    value_list = df.to_dict('records')
    final_list = list()

    for item in value_list:
        rec = list()
        for pair in item.items():
            rec.append(pair)
        final_list.append(rec)
    
    return final_list


def get_labels_for_table(df):
    print_list = ['FSN', 'Category', 'Brand', 'Color_code', 
    'Size in number', 'Article number', 'Net Quantity', 
    'Dimensions', 'MRP', 'Genric Name', 'Month & Year of Manufacturing', 
    'Manufactured by/', 'Marketed by/Customer Care', 'email', 'mobile']

    df_print = df[print_list]
    final_list = get_tupple_from_dataframe(df_print)

    buffer = io.BytesIO()
    c = canvas.Canvas(buffer, bottomup=0, pagesize=(150*mm,100*mm))
    
    for index, data in df.iterrows():
        list_to_print = final_list[index]
        for x in range(0, data.qty):
            c.setFont("Helvetica", 10)
            init_x = 10
            init_y = 20
            for item_list in list_to_print:
                label = str(item_list[0])
                value = str(item_list[1])
                
                if label not in ['Category', 'email', 'mobile']:
                    c.drawString(init_x, init_y, label)
                value_pos = init_x + 145

                change_value = 17
                if label == 'Brand':
                    c.setFont("Helvetica", 14)
                    change_value = 12
                
                if label == 'Color_code':
                    c.setFont("Helvetica", 11)
                    change_value = 15

                c.drawString(value_pos, init_y, value)
                c.setFont("Helvetica", 10)
                init_y = init_y + change_value
            c.line(150,10, 152, 260)
            c.showPage()
    c.save()
    buffer.seek(0)
    return buffer


def get_labels(data):
    buffer = io.BytesIO()
    data = data.fillna({'QTY': 0})
    c = canvas.Canvas(buffer, bottomup=0, pagesize=(250*mm,150*mm))

    for index, row in data.iterrows():
        mrp = '{:0,.2f}'.format(row.MRP)
        product_name = row.Product_Name if row.Product_Name else 'UnNamed Product'
        for i in range(0, int(row.QTY)):
            textobject = c.beginText()
            textobject.setTextOrigin(3*mm, 5*mm)
            textobject.setFont('Courier', 10)
            textobject.textLine(text='Big Fox - ' + product_name)
            c.drawText(textobject)
            textobject.setFont('Courier', 14)
            textobject.textLine(text=' ')
            c.drawText(textobject)
            textobject.textLine(text='SKU      : ' + str(row.SKU))
            c.drawText(textobject)
            textobject.textLine(text='Color    : ' + row.Color)
            c.drawText(textobject)
            textobject.textLine(text='Size     : ' + str(row.Size))
            c.drawText(textobject)
            textobject.textLine(text='Price    : ' + mrp)
            c.drawText(textobject)
            textobject.textLine(text=' ')
            c.drawText(textobject)
            textobject.setFont('Courier', 8)
            textobject.textLine(text='MARKED BY - SIDDHARTH SALES')
            c.drawText(textobject)
            textobject.textLine(text='E-MAIL: siddharthsales5@gmail.com')
            c.drawText(textobject)
            # qr_code
            qr_code = qr.QrCodeWidget(f'Big_FOX<{row.SKU}_{row.Product_Name}_{row.Color}_{row.Size}_{mrp}>')
            bounds = qr_code.getBounds()
            width = bounds[2] - bounds[0]
            height = bounds[3] - bounds[1]
            d = Drawing(20*mm, 20*mm, transform=[20./width,0,0,20./height,0,0])
            d.add(qr_code)
            renderPDF.draw(d, c,65*mm, 40*mm)
            c.showPage()
    c.save()
    buffer.seek(0)
    return buffer
